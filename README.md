# ECE 469 Operating Systems Engineering

## Partner
[Andrew Gan](https://gitlab.com/Andrew-Gan)

## Software tools:  
[DLXOS](https://users.soe.ucsc.edu/~elm/Software/Dlxos/dlxarch.shtml)
(Noted: It's very hard to make it works on any UNIX|like system because I believe the source code has some errors and it was only tested on Solaris 2.8 and Mac OS 10.8)

  
#### Lab1 | Intro to DLXOS and Trap   
Implement Unix like User syscall [Getpid](lab1/)

**References:**
N/A

#### Lab2 | Process Synchronization  
Implement [Condition Variables and Monitors](lab2/os/synch.c)

**References:**
* [Condition Variables for pthread](http://pages.cs.wisc.edu/~remzi/OSTEP/threads|cv.pdf)
* [Implementing Condition Variables](http://birrell.org/andrew/papers/ImplementingCVs.pdf)
* [Implementing Monitors](https://people.eecs.berkeley.edu/~kubitron/courses/cs162|F06/hand|outs/synch.html)

#### Lab3 | Message Passing and Process Scheduling  
Implement [Mailbox](lab3/os/mailbox.c) <br>
Implement [BSD 4.4 priority scheduling](lab3/os/process.c)

**References:**
N/A

#### Lab4 | Memory Management  
Implement [stack, code, data](lab4/one|level) <br>
Implement Unix like [fork](lab4/fork) <br>
Implement Unix like [heap](lab4/heap) <br>

**References:**
N/A

#### Lab5 | File System
Implement Unix like ext2 file system [flat](lab5/flat) and [multi level](lab5/multilevel) directory <br>

**References:**

* [Ext2](http://cs.smith.edu/~nhowe/Teaching/csc262/oldlabs/ext2.html)
* [Another Ex2](https://piazza.com/class_profile/get_resource/il71xfllx3l16f/inz4wsb2m0w2oz)

#### Bonus:
Implement [simple shell](lab5/multilevel/apps/ostests/ostests/ostests.c) for multi directory <br>


#### Available command: 
| Commands   |      Description      |  
|----------|:-------------:|
| addline [filename] < [newline] | add new line to file | 
|cat [filename] |  show the content of the file |
|cd |  change directory
|chmod [permission] |  change the permission
|clear |  clear the whole terminal screen
|delline [filename] |  delete last line in file |
|exit |  Exit the shell |
|ls |  list out files and directorys on the current path|
|mkdir [directory name] |  create a directory|
|pid |  print out the pid of the program|
|pwd |  print out current path of the program|
|rm [filename] |  delete a file |
|rmdir [directory name] |  remove a directory |
|source [filename] |  execute file as script |
|touch [filename] |  create a filename in the  |
  


### Demo: <br>
View file content: <br> 
![cat demo](cat_demo.gif) <br> <br> <br>
Change permission: <br>
![change permission demo](permission_demo.gif) <br> <br> <br>
Deletion: <br>
![deletion demo](remove_demo.gif) <br> <br> <br>
Creation: <br>
![creation demo](create_demo.gif) <br> <br> <br>