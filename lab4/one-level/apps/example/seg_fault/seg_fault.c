#include "usertraps.h"
#include "misc.h"

// This will be put somewhere in the middle of virtual memory , shift right is divided by 2
#define MEM_PAGESIZE                0x1000      
#define MEM_MAX_VIRTUAL_ADDRESS     0xfffff 

void main (int argc, char *argv[])
{
  sem_t s_procs_completed; // Semaphore to signal the original process that we're done
  unsigned int * next_page_addr;
  unsigned int   test;

  if (argc != 2) { 
    Printf("Usage: %s <handle_to_procs_completed_semaphore>\n"); 
    Exit();
  } 

  // Convert the command-line strings into integers for use as handles
  s_procs_completed = dstrtol(argv[1], NULL, 10);


    // Now print a message to show that everything worked
  Printf("PID %d: Trying to access address inside virtual address but hasn't been allocated!\n", getpid());

  // Signal the semaphore to tell the original process that we're done
  if(sem_signal(s_procs_completed) != SYNC_SUCCESS) {
    Printf("hello_world (%d): Bad semaphore s_procs_completed (%d)!\n", getpid(), s_procs_completed);
    Exit();
  }



  next_page_addr = (unsigned int *)(MEM_MAX_VIRTUAL_ADDRESS - MEM_PAGESIZE - 3)  ;
  test = *next_page_addr;
  Printf("%d\n", test);
  
}
