#include "usertraps.h"
#include "misc.h"

#define HELLO_WORLD "hello_world.dlx.obj"
#define PRINT_HELLO_WORLD "print_hello_world.dlx.obj"
#define MAX_VIRTUAL_ADDRESS "max_virtual_address.dlx.obj"
#define SEG_FAULT "seg_fault.dlx.obj"
#define GROW_STACK "grow_stack.dlx.obj"
#define LARGE_MEMORY_TEST "large_memory_test.dlx.obj"

void main (int argc, char *argv[])
{
  int num_hello_world = 0;             // Used to store number of processes to create
  int i;                               // Loop index variable
  sem_t s_procs_completed;             // Semaphore used to wait until all spawned processes have completed
  char s_procs_completed_str[10];      // Used as command-line argument to pass page_mapped handle to new processes

  if (argc != 2) {
    Printf("Usage: %s <number of hello world processes to create>\n", argv[0]);
    Exit();
  }

  // Convert string from ascii command line argument to integer number
  num_hello_world = dstrtol(argv[1], NULL, 10); // the "10" means base 10

  // Create semaphore to not exit this process until all other processes 
  // have signalled that they are complete.
  if ((s_procs_completed = sem_create(0)) == SYNC_FAIL) {
    Printf("makeprocs (%d): Bad sem_create\n", getpid());
    Exit();
  }


  // Setup the command-line arguments for the new processes.  We're going to
  // pass the handles to the semaphore as strings
  // on the command line, so we must first convert them from ints to strings.
  ditoa(s_procs_completed, s_procs_completed_str);


  // Create print Hello World processes
  Printf("-------------------------------------------------------------------------------------\n");
  Printf("makeprocs (%d): Creating printing helloworld process\n ", getpid());
  process_create(PRINT_HELLO_WORLD,s_procs_completed_str,NULL);

  if(sem_wait(s_procs_completed) != SYNC_SUCCESS) {
     Printf("Bad semaphore s_procs_completed (%d) in %s\n", s_procs_completed, argv[0]);
     Exit();
  }
  Printf("-------------------------------------------------------------------------------------\n");



  //Create a process that will cause seg fault
  Printf("-------------------------------------------------------------------------------------\n");
  Printf("makeprocs (%d): Creating a process that will cause segmentation fault\n ", getpid());
  process_create(SEG_FAULT,s_procs_completed_str,NULL);

  if(sem_wait(s_procs_completed) != SYNC_SUCCESS) {
     Printf("Bad semaphore s_procs_completed (%d) in %s\n", s_procs_completed, argv[0]);
     Exit();
  }
  Printf("-------------------------------------------------------------------------------------\n");


  // Create a process that will grow stack
  Printf("-------------------------------------------------------------------------------------\n");
  Printf("makeprocs (%d): Creating a process that will grow stack\n ", getpid());
  process_create(GROW_STACK,s_procs_completed_str,NULL);

  if(sem_wait(s_procs_completed) != SYNC_SUCCESS) {
     Printf("Bad semaphore s_procs_completed (%d) in %s\n", s_procs_completed, argv[0]);
     Exit();
  }
  Printf("-------------------------------------------------------------------------------------\n");


  // Create Hello World processes
  Printf("-------------------------------------------------------------------------------------\n");
  Printf("makeprocs (%d): Creating %d hello world's in a row, but only one runs at a time\n", getpid(), num_hello_world);
  for(i=0; i<num_hello_world; i++) {
    Printf("makeprocs (%d): Creating hello world #%d\n", getpid(), i);
    process_create(HELLO_WORLD, s_procs_completed_str, NULL);
    if (sem_wait(s_procs_completed) != SYNC_SUCCESS) {
      Printf("Bad semaphore s_procs_completed (%d) in %s\n", s_procs_completed, argv[0]);
      Exit();
    }
  }

  // Create semaphore to not exit this process until all other processes 
  // have signalled that they are complete.
  if ((s_procs_completed = sem_create(-29)) == SYNC_FAIL) {
    Printf("makeprocs (%d): Bad sem_create\n", getpid());
    Exit();
  }


  // Setup the command-line arguments for the new processes.  We're going to
  // pass the handles to the semaphore as strings
  // on the command line, so we must first convert them from ints to strings.
  ditoa(s_procs_completed, s_procs_completed_str);

  Printf("makeprocs (%d): Creating %d 30 processes\n", getpid());
  // Create Hello World processes
  Printf("-------------------------------------------------------------------------------------\n");
  for(i=0; i<30; i++) {
    Printf("makeprocs (%d): Created #%d processes\n", getpid(), i+1);
    process_create(LARGE_MEMORY_TEST, s_procs_completed_str, NULL);
   
  }

  if (sem_wait(s_procs_completed) != SYNC_SUCCESS) {
      Printf("Bad semaphore s_procs_completed (%d) in %s\n", s_procs_completed, argv[0]);
      Exit();
  }

  // Create semaphore to not exit this process until all other processes 
  // have signalled that they are complete.
  if ((s_procs_completed = sem_create(0)) == SYNC_FAIL) {
    Printf("makeprocs (%d): Bad sem_create\n", getpid());
    Exit();
  }

  // Create outside max virtual address processes
  Printf("-------------------------------------------------------------------------------------\n");
  Printf("makeprocs (%d): Creating accessing outside max virtual address process\n ", getpid());
  process_create(MAX_VIRTUAL_ADDRESS,s_procs_completed_str,NULL);

  if(sem_wait(s_procs_completed) != SYNC_SUCCESS) {
     Printf("Bad semaphore s_procs_completed (%d) in %s\n", s_procs_completed, argv[0]);
     Exit();
  }
  Printf("-------------------------------------------------------------------------------------\n");

  Printf("-------------------------------------------------------------------------------------\n");
  Printf("makeprocs (%d): All other processes completed, exiting main process.\n", getpid());

}
