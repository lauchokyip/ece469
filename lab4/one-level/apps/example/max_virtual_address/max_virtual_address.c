#include "usertraps.h"
#include "misc.h"

#define OUT_MEM_MAX_VIRTUAL_ADDRESS    (0xfffff + 1)            // (1 MB)   -> (2^20 - 1)

void main (int argc, char *argv[])
{
  sem_t s_procs_completed; // Semaphore to signal the original process that we're done
  unsigned int * outside_virtual_address;
  unsigned int   test;

  if (argc != 2) { 
    Printf("Usage: %s <handle_to_procs_completed_semaphore>\n"); 
    Exit();
  } 

  // Convert the command-line strings into integers for use as handles
  s_procs_completed = dstrtol(argv[1], NULL, 10);


  
  // Now print a message to show that everything worked
  Printf("PID %d: Trying to access address exceed virtual address!\n", getpid());
  outside_virtual_address = (unsigned int *)OUT_MEM_MAX_VIRTUAL_ADDRESS;
  test = *outside_virtual_address;
  Printf("%d\n", test);

  // Signal the semaphore to tell the original process that we're done
  if(sem_signal(s_procs_completed) != SYNC_SUCCESS) {
    Printf("hello_world (%d): Bad semaphore s_procs_completed (%d)!\n", getpid(), s_procs_completed);
    Exit();
  }

  
}
