#include "usertraps.h"
#include "misc.h"


void growstack(int first_page){
    int which_page;
    //add more variables so it will allocate more space for variable
    long long int extra_1=0xFFFFFFFF;
    long long int extra_2=0xFFFFFFFF;
    long long int extra_3=0xFFFFFFFF;
    long long int extra_4=0xFFFFFFFF;
    long long int extra_5=0xFFFFFFFF;
    long long int extra_6=0xFFFFFFFF;
    long long int extra_7=0xFFFFFFFF;
    long long int extra_8=0xFFFFFFFF;
    long long int extra_9=0xFFFFFFFF;
    long long int extra_10=0xFFFFFFFF;

    which_page = (int)(&which_page);
    which_page = (which_page >> 12);

    if(which_page < first_page)
    {
        Printf("In next page which has page number %d!\n", which_page);
        return;
    } 

    Printf("Still in first page, which has pagenum %d....\n",first_page);
    growstack(first_page);

}            

void main (int argc, char *argv[])
{
  sem_t s_procs_completed; // Semaphore to signal the original process that we're done
  int   first_page;

  if (argc != 2) { 
    Printf("Usage: %s <handle_to_procs_completed_semaphore>\n"); 
    Exit();
  } 

  // Convert the command-line strings into integers for use as handles
  s_procs_completed = dstrtol(argv[1], NULL, 10);

  first_page = (int) (&first_page);
  first_page = first_page >> 12;
  growstack(first_page);


  // Signal the semaphore to tell the original process that we're done
  if(sem_signal(s_procs_completed) != SYNC_SUCCESS) {
    Printf("hello_world (%d): Bad semaphore s_procs_completed (%d)!\n", getpid(), s_procs_completed);
    Exit();
  }

  
}
