#include "usertraps.h"
#include "misc.h"

#define FORK "test_fork.dlx.obj"
#define HELLOWORLD "print_hello_world.dlx.obj"


void main (int argc, char *argv[])
{
  int i;                               // Loop index variable
  sem_t s_procs_completed;             // Semaphore used to wait until all spawned processes have completed
  char s_procs_completed_str[10];      // Used as command-line argument to pass page_mapped handle to new processes
  int *test;
  int *test1;
  int *test2;

  if (argc != 1) {
    Printf("No arguements are needed!\n", argv[0]);
    Exit();
  }



  test = malloc(40);
  if(test == NULL){
    Printf("malloc returned NULL\n");
    Exit();
  }
  Printf("Allocated address is %d\n",(int)test);

  
  test1 = malloc(32);
  if(test1 == NULL){
    Printf("malloc returned NULL\n");
    Exit();
  }
  Printf("Allocated address is %d\n",(int)test1);

  test2 = malloc(80);
  if(test2 == NULL){
    Printf("malloc returned NULL\n");
    Exit();
  }
  Printf("Allocated address is %d\n",(int)test2);
  
  mfree(test1);
  mfree(test2); 
  mfree(test);

  // test = malloc(2048);
  // if(test == NULL){
  //   Printf("malloc returned NULL\n");
  //   Exit();
  // }
  // Printf("Allocated address is %d\n",(int)test);



}
