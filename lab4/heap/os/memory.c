//
//	memory.c
//
//	Routines for dealing with memory management.

//static char rcsid[] = "$Id: memory.c,v 1.1 2000/09/20 01:50:19 elm Exp elm $";

#include "ostraps.h"
#include "dlxos.h"
#include "process.h"
#include "memory.h"
#include "queue.h"

static uint32 freemap[MEM_FREEMAP_SIZE];
static uint32 pagestart;
static int nfreepages;
static int freemapmax;

//array to keep track each physical memory how many processes are using it
static int page_refcounter[MEM_L1FIELD_PAGE_TABLE_SIZE];

//----------------------------------------------------------------------
//
//	This silliness is required because the compiler believes that
//	it can invert a number by subtracting it from zero and subtracting
//	an additional 1.  This works unless you try to negate 0x80000000,
//	which causes an overflow when subtracted from 0.  Simply
//	trying to do an XOR with 0xffffffff results in the same code
//	being emitted.
//
//----------------------------------------------------------------------
static int negativeone = 0xFFFFFFFF;
inline uint32 invert (uint32 n) {
  return (n ^ negativeone);
}

static inline int FindMax(int x, int y){
  if(x > y){
    return x;
  }
  return y;
}

//----------------------------------------------------------------------
//
//	MemoryGetSize
//
//	Return the total size of memory in the simulator.  This is
//	available by reading a special location.
//
//----------------------------------------------------------------------
int MemoryGetSize() {
  return (*((int *)DLX_MEMSIZE_ADDRESS));
}

inline
void
MemorySetFreemap (int page_num, int bitset)
{
  uint32	which_freemap_index = page_num / 32;
  uint32	bitnum = page_num % 32;

  freemap[which_freemap_index] = (freemap[which_freemap_index] & invert((uint32)1 << bitnum)) | (bitset << bitnum);
  dbprintf ('m', "Set freemap entry %d to 0x%x.\n",
	    which_freemap_index, freemap[which_freemap_index]);
}

//----------------------------------------------------------------------
//
//	MemoryModuleInit
//
//	Initialize the memory module of the operating system.
//      Basically just need to setup the freemap for pages, and mark
//      the ones in use by the operating system as "VALID", and mark
//      all the rest as not in use.
//
//----------------------------------------------------------------------
void MemoryModuleInit() {
  int i;
  int curpage;
  
  // find the page for user program to start, 
  // clear LSB 2 bits to make sure it's 4 byte aligned
  pagestart = ((lastosaddress + MEM_PAGESIZE) & invert((uint32) 0x3)) / MEM_PAGESIZE; 
  dbprintf ('m', "Map has %d entries, memory size is 0x%x.\n",
	    freemapmax, MEM_MAX_SIZE / MEM_PAGESIZE);
  dbprintf ('m', "Free pages start with page # 0x%x.\n", pagestart);
  
  
  //Intialize all the freemap by setting all pages to used
  for (i = 0; i < MEM_FREEMAP_SIZE; i++) freemap[i] = PAGE_IN_USE;
  nfreepages = 0;
  
  // set all pages not used by OS as not in use
  for (curpage = pagestart; curpage < (MEM_MAX_SIZE / MEM_PAGESIZE); curpage++) {
    nfreepages += 1;
    MemorySetFreemap(curpage, PAGE_NOT_IN_USE);
  }

  dbprintf ('m', "Initialized %d free pages.\n", nfreepages);

  //os page as use by OS
  for(i = 0; i < pagestart; i++) 
    page_refcounter[i] = 1;

  //set up page reference counter
  for(; i < MEM_L1FIELD_PAGE_TABLE_SIZE; i++){
    page_refcounter[i] = 0;
  }
  dbprintf('m', "Initialized page_refcounter array from 0 to %d set to 1\n",pagestart);
}


//----------------------------------------------------------------------
//
// MemoryTranslateUserToSystem
//
//	Translate a user address (in the process referenced by pcb)
//	into an OS (physical) address.  Return the physical address.
//
//----------------------------------------------------------------------
uint32 MemoryTranslateUserToSystem (PCB *pcb, uint32 addr) {
  // get page number and offset within page from address
  int	virtual_page_num = addr >> MEM_L1FIELD_FIRST_BITNUM;
  int offset = addr & (MEM_PAGESIZE - 1);

  // Check if the addr is greater than max virtual address
  if(addr > MEM_MAX_VIRTUAL_ADDRESS){
    printf("FATAL ERROR: In  MemoryTranslateUserToSystem given address is larger than max virtual address\n");
    ProcessKill();
    return MEM_FAIL;
  }

  if((pcb->pagetable[virtual_page_num] & MEM_PTE_VALID) == 0)
  {
    dbprintf('m',"Page Fault when translating!\n");
    MemoryPageFaultHandler(pcb);
  }

  // remove status bits from pte and return only page + offset
  dbprintf('m',"Translated User to Physical 0x%x  \n",addr);
  dbprintf('m',"The pagetable value is %x \n",pcb->pagetable[virtual_page_num]);
  return ((pcb->pagetable[virtual_page_num] & MEM_PTE_MASK) | offset);
}

//----------------------------------------------------------------------
//
//	MemoryMoveBetweenSpaces
//
//	Copy data between user and system spaces.  This is done page by
//	page by:
//	* Translating the user address into system space.
//	* Copying all of the data in that page
//	* Repeating until all of the data is copied.
//	A positive direction means the copy goes from system to user
//	space; negative direction means the copy goes from user to system
//	space.
//
//	This routine returns the number of bytes copied.  Note that this
//	may be less than the number requested if there were unmapped pages
//	in the user range.  If this happens, the copy stops at the
//	first unmapped address.
//
//----------------------------------------------------------------------
int MemoryMoveBetweenSpaces (PCB *pcb, unsigned char *system, unsigned char *user, int n, int dir) {
  unsigned char *curUser;         // Holds current physical address representing user-space virtual address
  int		bytesCopied = 0;  // Running counter
  int		bytesToCopy;      // Used to compute number of bytes left in page to be copied

  while (n > 0) {
    // Translate current user page to system address.  If this fails, return
    // the number of bytes copied so far.
    curUser = (unsigned char *)MemoryTranslateUserToSystem (pcb, (uint32)user);

    // If we could not translate address, exit now
    if (curUser == (unsigned char *)0) break;

    // Calculate the number of bytes to copy this time.  If we have more bytes
    // to copy than there are left in the current page, we'll have to just copy to the
    // end of the page and then go through the loop again with the next page.
    // In other words, "bytesToCopy" is the minimum of the bytes left on this page 
    // and the total number of bytes left to copy ("n").

    // First, compute number of bytes left in this page.  This is just
    // the total size of a page minus the current offset part of the physical
    // address.  MEM_PAGESIZE should be the size (in bytes) of 1 page of memory.
    // MEM_ADDRESS_OFFSET_MASK should be the bit mask required to get just the
    // "offset" portion of an address.
    bytesToCopy = MEM_PAGESIZE - ((uint32)curUser & MEM_ADDRESS_OFFSET_MASK);
    
    // Now find minimum of bytes in this page vs. total bytes left to copy
    if (bytesToCopy > n) {
      bytesToCopy = n;
    }

    // Perform the copy.
    if (dir >= 0) {
      bcopy (system, curUser, bytesToCopy);
    } else {
      bcopy (curUser, system, bytesToCopy);
    }

    // Keep track of bytes copied and adjust addresses appropriately.
    n -= bytesToCopy;           // Total number of bytes left to copy
    bytesCopied += bytesToCopy; // Total number of bytes copied thus far
    system += bytesToCopy;      // Current address in system space to copy next bytes from/into
    user += bytesToCopy;        // Current virtual address in user space to copy next bytes from/into
  }
  return (bytesCopied);
}

//----------------------------------------------------------------------
//
//	These two routines copy data between user and system spaces.
//	They call a common routine to do the copying; the only difference
//	between the calls is the actual call to do the copying.  Everything
//	else is identical.
//
//----------------------------------------------------------------------
int MemoryCopySystemToUser (PCB *pcb, unsigned char *from,unsigned char *to, int n) {
  return (MemoryMoveBetweenSpaces (pcb, from, to, n, 1));
}

int MemoryCopyUserToSystem (PCB *pcb, unsigned char *from,unsigned char *to, int n) {
  return (MemoryMoveBetweenSpaces (pcb, to, from, n, -1));
}

//---------------------------------------------------------------------
// You may need to implement the following functions and access them from process.c
// Feel free to edit/remove them
//---------------------------------------------------------------------

int MemoryAllocPage(void) {
  static int	mapnum = 0;
  int		bitnum;
  uint32  available_freemap;
  uint32	available_page_index;

  //if there is no free page
  if (nfreepages == 0) {
    return MEM_FAIL;
  }
  dbprintf ('m', "Allocating physical memory, starting with page %d\n", mapnum);
  // loop through freemap to search for page not in use
  while (freemap[mapnum] == PAGE_IN_USE) {
    mapnum += 1;
    if (mapnum >= MEM_FREEMAP_SIZE) mapnum = 0;
  }
  //Found the available freemap!
  available_freemap = freemap[mapnum];
  // Find exact bit in uint32 that represents the page not in use
  for (bitnum = 0; (available_freemap & (1 << bitnum)) == 0; bitnum++) {}
  //clear the freemap bit as inused
  freemap[mapnum] &= invert((uint32)(1 << bitnum));
  //convert it to page index
  available_page_index = (mapnum * 32) + bitnum;
  dbprintf ('m', "Allocated memory, from map %d, page %d, map=0x%x.\n",
	    mapnum, available_page_index, freemap[mapnum]);
  // decrease number of free pages
  nfreepages -= 1;
  // now set the page used by 1 process
  page_refcounter[available_page_index]++;
  dbprintf('m',"In MemoryAlloc Page: Page %d has %d process using\n",
      available_page_index,page_refcounter[available_page_index]);

  return available_page_index;
}


//---------------------------------------------------------------------
// MemoryPageFaultHandler is called in traps.c whenever a page fault 
// (better known as a "seg fault" occurs.  If the address that was
// being accessed is on the stack, we need to allocate a new page 
// for the stack.  If it is not on the stack, then this is a legitimate
// seg fault and we should kill the process.  Returns MEM_SUCCESS
// on success, and kills the current process on failure.  Note that
// fault_address is the beginning of the page of the virtual address that 
// caused the page fault, i.e. it is the vaddr with the offset zero-ed
// out.
//
// Note: The existing code is incomplete and only for reference. 
// Feel free to edit.
//---------------------------------------------------------------------
int MemoryPageFaultHandler(PCB *pcb) {
  
  uint32 fault_address;
  uint32 user_stack_ptr;
  uint32 pagenum_fault_addr;
  uint32 pagenum_user_stack;
  uint32 physical_pagenum_allocated; 

  dbprintf('m', "Entering Memory Fault Handler..\n");

  //grab faulting addr (the comments mentioned it's offset 0 ?)
  fault_address = pcb->currentSavedFrame[PROCESS_STACK_FAULT];
  //get user stack address
  user_stack_ptr = pcb->currentSavedFrame[PROCESS_STACK_USER_STACKPOINTER];
  // find pagenum for faulting address
  pagenum_fault_addr = fault_address >> MEM_L1FIELD_FIRST_BITNUM;
  // find pagenum for the user stack
  pagenum_user_stack = user_stack_ptr >> MEM_L1FIELD_FIRST_BITNUM;

  // if it's segfault,  
  if(pagenum_fault_addr < pagenum_user_stack){
    printf("FATAL ERROR: SEGFAULT! Going to kill the Process! \n");
    ProcessKill();
    return MEM_FAIL;
  }
  // This process uses more than 32 pages
  if((pcb->npages + 1) >= 32){
      printf("FATAL: Each Process can only have 32 physical pages allocated \n");
      ProcessKill();
      return MEM_FAIL;

  }
    //allocate page
   physical_pagenum_allocated = MemoryAllocPage();
    //if physical page is full
    if(physical_pagenum_allocated == MEM_FAIL) 
    {
      printf("FATAL: not enough physical memory to be allocated! \n");
      ProcessKill();
      return MEM_FAIL;
    }
    // it's not full
    dbprintf('m',"In ROP handler: Allocated physical memory, with page number: %d\n",physical_pagenum_allocated);
    // assign the page to page table
    pcb->pagetable[pagenum_fault_addr] = MemorySetupPte(physical_pagenum_allocated);
    // allocated one more page
    pcb->npages++;

    return MEM_SUCCESS;
 
}

//----------------------------------------------------------------------
//
// MemoryROPAccessHandler
//
// This function is called whenever a process tries to access a page that is marked as “readonly”
// in its page table. If no one else refers to this page (refcounter == 1), then we can just mark it as
// READONLY in the page table. Otherwise, we have to allocate a new page, copy the entire old page to the
// new page, decrement the reference coutner for the old page, mark the new page as READONLY, and put
// the new pte into the page table to replace the old pte */
//
//----------------------------------------------------------------------
int MemoryROPAccessHandler(PCB *pcb){
  uint32 fault_addr;
  uint32 pagenum_fault_addr;
  int old_pagenum_physical;
  int physical_pagenum_allocated;

  // find the address that throws the exception
  fault_addr = pcb->currentSavedFrame[PROCESS_STACK_FAULT];
  dbprintf('m', "Fault address is 0x%x\n",fault_addr);
  // find the page number of this fault address
  pagenum_fault_addr = fault_addr >> MEM_L1FIELD_FIRST_BITNUM;
  // check the PTE to find the physical page
  old_pagenum_physical = ((pcb->pagetable[pagenum_fault_addr]) >> MEM_L1FIELD_FIRST_BITNUM);

  // kill the process if there is no process using it
  if(page_refcounter[old_pagenum_physical] < 1){
    printf("FATAL ERROR: No process is using the pagenum %d, kill it!\n",old_pagenum_physical);
    ProcessKill();
    return MEM_FAIL;
  }
  //This mean nobody is using it, just change the PTE
  if(page_refcounter[old_pagenum_physical] == 1){
     printf("There is the only process using the page, clearing ROP bit\n");
     pcb->pagetable[pagenum_fault_addr] &= invert((uint32) MEM_PTE_READONLY);
     dbprintf('m', "Checking if we clear it by printing 0x%x\n",pcb->pagetable[pagenum_fault_addr] );

  }
  else {
      dbprintf('m', "More than one page using the same page %d\n",old_pagenum_physical);
      //There are more than 1 processes using it
      physical_pagenum_allocated = MemoryAllocPage();
      if(physical_pagenum_allocated == MEM_FAIL){
        printf("FATAL: not enough physical memory to be allocated! \n");
        ProcessKill();
        return MEM_FAIL;
      }
      dbprintf('m', "New page %d is allocated \n",physical_pagenum_allocated);
      //copy for fault addr to new allocated page (Not sure )
      bcopy((char *)(old_pagenum_physical       << MEM_L1FIELD_FIRST_BITNUM), 
            (char *)(physical_pagenum_allocated << MEM_L1FIELD_FIRST_BITNUM), 
            MEM_PAGESIZE);
      //Befores setting up PTE
      printf("ROP handler: old valid page table entry is %x\n", pcb->pagetable[pagenum_fault_addr] );
      pcb->pagetable[pagenum_fault_addr] = MemorySetupPte(physical_pagenum_allocated);
      //After setting up PTE
      printf("ROP handler: new valid page table entry is %x\n", pcb->pagetable[pagenum_fault_addr] );
      // Decrease page_refcounter for old physical table
      page_refcounter[old_pagenum_physical]--;
  }

  dbprintf('m', "Leaving ROP Access Handler..\n");
  return MEM_SUCCESS;
}

//----------------------------------------------------------------------
//
// MemoryFreePte
//
//      Free a page given its PTE.
//
//----------------------------------------------------------------------
int 
MemoryFreePte (uint32 pte)
{
  uint32 page_num = pte >> MEM_L1FIELD_FIRST_BITNUM;
  dbprintf('m',"Trying to free physical page number %d in MemoryFreePte!\n",page_num);
  //If no process is using, why do we want to free it?
  if(page_refcounter[page_num] < 1) {
    printf("PID %d FATAL ERROR: No process is using the page, no reason to free it\n",GetCurrentPid());
    ProcessKill();
    return MEM_FAIL;
  }

  //Decrease the number of pages using it
  page_refcounter[page_num]--;

  //If no process is using it anymore
  if(page_refcounter[page_num] == 0) 
    MemoryFreePage (page_num);

  return MEM_SUCCESS;
}

//----------------------------------------------------------------------
//
// MemorySetupShare
//
//	Allocate a new pagetable for the process.
//
//----------------------------------------------------------------------
uint32 MemorySetupShare (PCB *pcb, uint32 virtual_page_num){
  uint32 physical_page_num;
  uint32 new_PTE_value;

  physical_page_num = (pcb->pagetable[virtual_page_num] >> MEM_L1FIELD_FIRST_BITNUM);
  dbprintf('m', "Setting up pagetable to READONLY for physical page %d\n",physical_page_num);
  page_refcounter[physical_page_num]++;
  
  new_PTE_value = (pcb->pagetable[virtual_page_num] | MEM_PTE_READONLY);
  dbprintf('m', "New page table value is 0x%x\n",new_PTE_value);
  return new_PTE_value;
}



//----------------------------------------------------------------------
//
// MemorySetupPte
//
//	Set up a PTE given a physical page number.
//
//----------------------------------------------------------------------
uint32 MemorySetupPte (uint32 page_num) {
  return ((page_num << MEM_L1FIELD_FIRST_BITNUM) | MEM_PTE_VALID);
}

//----------------------------------------------------------------------
//
//	MemoryFreePage
//
//	Free a page of memory.
//
//----------------------------------------------------------------------
void MemoryFreePage(uint32 page_num) {
  // set page to not in use
  MemorySetFreemap(page_num, PAGE_NOT_IN_USE);
  // increment number of free pages
  nfreepages += 1;
  dbprintf ('m',"Freed page 0x%x, %d remaining.\n", page_num, nfreepages);
}

//----------------------------------------------------------------------
//
//	MemSizeToOrder
//
//	
//
//----------------------------------------------------------------------
static int MemSizeToOrder(int memsize){
  int order;

  if(memsize > MEM_PAGESIZE){
    printf("FATAL ERROR: The memsize %d exceed the order when translating\n",memsize);
    return MEM_FAIL;
  }
  if(memsize <= 0 ){
     printf("FATAL ERROR: The memsize is too small\n");
     return MEM_FAIL;
  }

  //Keep divide it by 2 until we find the order
  order = 0;
  while(memsize > MEM_HEAP_MIN_SIZE ){
    memsize >>= 1;
    order++;
  }

  return order;
}

//----------------------------------------------------------------------
//
//	Fix tree for malloc
//
//	Fix the "heap-like tree" for pcb->heaparr
//
//----------------------------------------------------------------------
static void MallocUpwardFixTree(PCB *pcb, int arr_index, int which_order){

    int i;
    int parent_index;
    int left_child;
    int right_child;
    int max;

    dbprintf('m',"Fixing the tree...\n");
    for(i = which_order ; i < MEM_MAX_HEAP_ORDER; i++){
      parent_index = PARENT(arr_index);
      left_child  = pcb -> heap_block[LCHILD(parent_index)].num_available;
      right_child = pcb -> heap_block[RCHILD(parent_index)].num_available;
      
      //Find the max
      max = FindMax(left_child,right_child);
      dbprintf('m',"The left child %d right child %d and max is %d\n",left_child,right_child,max);

      //overwrite parent's max value
      pcb -> heap_block[parent_index].num_available = max;
      // change arr_index to parent_index until we reach top order
      arr_index = parent_index;
    }
}

//----------------------------------------------------------------------
//
//	malloc
//
//	allocate memory.
//
//----------------------------------------------------------------------

void *malloc(PCB *pcb, int memsize){
  int i;
  int order_to_look_for;
  uint32 right_address_offset;
  uint32 running_address_offset;
  int left_num_available;
  int index;
  
  // find the order of the memory
  order_to_look_for = MemSizeToOrder(memsize);
  dbprintf('m',"The order to look for is %d\n",order_to_look_for);
  if(order_to_look_for == MEM_FAIL){
    printf("FATAL Error: Fail at allocating memory\n");
    printf("Exiting malloc...\n");
    return NULL;
  }

  dbprintf('m',"Checking root size %d\n",pcb->heap_block[0].num_available);

  index = 0;
  right_address_offset = 0x1000;
  running_address_offset = 0;

    // the root will always have the largest order that can be allocated
  if(pcb->heap_block[index].num_available < order_to_look_for){
      dbprintf('m',"root num is %d but order to look for is %d\n",
              pcb->heap_block[0].num_available,order_to_look_for);
      printf("FATAL Error: No space left to be allocated\n");
      return NULL;
  }

  // Now search the available memory (only 7 layer avaialble)
  for (i = MEM_MAX_HEAP_ORDER; i > order_to_look_for ; i-- ){

    //in case we need to use it
    right_address_offset >>= 1;

    // There is space but root is not splited
    if(pcb->heap_block[index].is_spilit == false){
      pcb->heap_block[index].offset = running_address_offset;
      // split it
      pcb->heap_block[index].is_spilit = true;

      // printing statement for left child
      printf("Creating a left child node (order = %d, ", (i-1));
      printf("addr = %d, ",running_address_offset);
      printf("size = %d) ", ((MEM_HEAP_MIN_SIZE) << (i-1)));
      printf("of parent (order = %d, ", (i));
      printf("addr = %d, ",running_address_offset);
      printf("size = %d) \n", ((MEM_HEAP_MIN_SIZE) << i));

      //allocate left child
      pcb->heap_block[LCHILD(index)].offset = running_address_offset;

      // printing statement for right child
      printf("Creating a right child node (order = %d, ", (i-1));
      printf("addr = %d, ",(running_address_offset + right_address_offset));
      printf("size = %d) ", ((MEM_HEAP_MIN_SIZE) << (i-1)));
      printf("of parent (order = %d, ", (i));
      printf("addr = %d, ",running_address_offset);
      printf("size = %d) \n", ((MEM_HEAP_MIN_SIZE) << i));

      //allocate right child
      pcb->heap_block[RCHILD(index)].offset = running_address_offset + right_address_offset;
   
   }

    //now check for available space for left child
    left_num_available = pcb->heap_block[LCHILD(index)].num_available;

    //go left if there is space
    if(left_num_available >= order_to_look_for){
      //Go left!
      index = LCHILD(index);
      dbprintf('m',"Going left\n"); 
    }
    else{
      //Go Right!
      index = RCHILD(index);
      running_address_offset += right_address_offset;
      dbprintf('m',"Going Right\n");
    }    
  }

  //Found where we have to locate
  pcb->heap_block[index].num_available = false;
  // We are using it so set the flag 
  pcb->heap_block[index].is_used = true;
  // set up it's offset
  pcb->heap_block[index].offset = running_address_offset;

  MallocUpwardFixTree(pcb,index,order_to_look_for);
  //Printing out allocated block
  printf("Allocated the block order %d, ",order_to_look_for);
  printf("addr = %d, ",pcb->heap_block[index].offset);
  printf("requested mem size = %d, ", memsize);
  printf("block size = %d\n", (MEM_HEAP_MIN_SIZE << order_to_look_for));

  return (void *)((4 << MEM_L1FIELD_FIRST_BITNUM) + pcb->heap_block[index].offset);
}

//----------------------------------------------------------------------
//
//	Fix tree for mfree
//
//	Fix the "heap-like tree" for pcb->heaparr
//
//----------------------------------------------------------------------
static void MfreeUpwardFixTree(PCB *pcb, int arr_index, int which_order){

    int i;
    Node *parent;
    Node *left_child;
    Node *right_child;
    int parent_index;
    int original_num;
    int max;

    dbprintf('m',"Fixing the tree...\n");
    
    for(i = which_order ; i < MEM_MAX_HEAP_ORDER; i++){
      parent_index = PARENT(arr_index);
      parent      = &(pcb -> heap_block[parent_index]);
      left_child  = &(pcb -> heap_block[LCHILD(parent_index)]);
      right_child = &(pcb -> heap_block[RCHILD(parent_index)]);
      
      // This is what the child order originally is
      original_num = i;
      
      // if both child has the order that it's supposed to have that means we have to
      // join them together
      if(left_child->num_available == original_num && 
        right_child->num_available == original_num )
      {  

        //print statement
        printf("Coalesced buddy nodes (order = %d, ", original_num);
        printf("addr = %d, ", left_child->offset);
        printf("size = %d) ", ((MEM_HEAP_MIN_SIZE) << original_num));
        printf("& (order = %d, ", original_num);
        printf("addr = %d, ", right_child->offset);
        printf("size = %d) \n", ((MEM_HEAP_MIN_SIZE) << original_num));

        left_child -> offset = 0;
        right_child -> offset = 0;

        printf("into the parent node (order = %d, ", (original_num + 1));
        printf("addr = %d, ", parent->offset);
        printf("size = %d) \n", (MEM_HEAP_MIN_SIZE << (original_num + 1)));

        parent->is_spilit = false;
        parent->num_available = (original_num + 1);
      }
      else{
      //Find the max
      max = FindMax(left_child->num_available,right_child->num_available);
      dbprintf('m',"The left child %d right child %d and max is %d\n"
                ,left_child -> num_available,right_child-> num_available,max);
      
      //overwrite parent's max value
      parent -> num_available = max;
      
      }

      // change arr_index to parent_index until we reach top order
      arr_index = parent_index;
      dbprintf('m',"Going up! \n");
    }
    dbprintf('m',"Reached the root...\n");
}

//----------------------------------------------------------------------
//
//	mfree
//
//	Fix the allocated heap space
//
//----------------------------------------------------------------------
int mfree(PCB *pcb, void *ptr){
  uint32 target_addrees = (int) ptr;
  uint32 base_address;
  uint32 offset;
  int order_of_target;
  int index;

  dbprintf('m',"In mfree to free allocated heap memory\n");
  //find offset of the address first
  base_address = (4 << MEM_L1FIELD_FIRST_BITNUM);
  offset = target_addrees - base_address;
  dbprintf('m',"target address is %d, base address is %d ",
                  (int)target_addrees, (int)base_address);
  dbprintf('m',"The offset is %d\n",(int)offset);
  //start looking from root
  index = 0;
  order_of_target = MEM_MAX_HEAP_ORDER;
  while (pcb->heap_block[index].is_used == false){
    // smaller than right offset then go left
    if(offset < pcb->heap_block[RCHILD(index)].offset){
      dbprintf('m',"Going left to find it!\n");
      index = LCHILD(index);
    }
    else
    {
      dbprintf('m',"Going right to find it!\n");
      index = RCHILD(index); 
    }
  
    order_of_target--;
    //if order of target is larger than 8 then it's no where
    //to be found
    if(order_of_target == false){
      printf("FATAL ERROR: Can't find the memory that you are looking for!\n");
      return MEM_FAIL;
    }
  }
  //At this point we have found the location that we need to fix!
  
  //if it has freed, why do we need to free again?
  if(pcb -> heap_block[index].is_used == false){
    printf("Already freed, don't need to be freed again\n");
    return MEM_FAIL;
  }

  //Printing out allocated block
  printf("Freed the block: order %d, ",order_of_target);
  printf("addr = %d, ",pcb->heap_block[index].offset);
  printf("size = %d\n", (MEM_HEAP_MIN_SIZE << order_of_target));
  
  dbprintf('m',"Freeing it now!\n");
  pcb -> heap_block[index].num_available = order_of_target;
  pcb -> heap_block[index].is_used = false;
  dbprintf('m',"Freed!\n");

  //Fix the tree since we have freed it, let the function do it for us!
  MfreeUpwardFixTree(pcb,index,order_of_target);

  //Free how many space is allocated
  return (MEM_HEAP_MIN_SIZE << order_of_target);
}
