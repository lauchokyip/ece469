#include "usertraps.h"
#include "misc.h"

#define FORK "test_fork.dlx.obj"
#define HELLOWORLD "print_hello_world.dlx.obj"


void main (int argc, char *argv[])
{
  int i;                               // Loop index variable
  sem_t s_procs_completed;             // Semaphore used to wait until all spawned processes have completed
  char s_procs_completed_str[10];      // Used as command-line argument to pass page_mapped handle to new processes
  int child_pid;

  if (argc != 1) {
    Printf("No arguements are needed!\n", argv[0]);
    Exit();
  }




  Printf("The main program has process ID %d\n",getpid());
  child_pid = fork();
  if(child_pid != 0){
    Printf("this is the parent process, with id %d\n",(int) getpid());
    Printf("the child's process ID is %d\n",child_pid);
  }
  
  else{
    Printf("this is the child process, with id %d\n",(int)getpid());

  }





}
