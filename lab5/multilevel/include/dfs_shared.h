#ifndef __DFS_SHARED__
#define __DFS_SHARED__

typedef struct dfs_superblock {
  // STUDENT: put superblock internals here
  unsigned char valid;           // a valid indicator for the file system
  unsigned int fs_blocksize;   // the file system block size
  unsigned int fs_numblocks;      // the total number of file system blocks
  unsigned int inode_start_fsblock_num;   // the starting file system block number for the array of inodes
  unsigned int num_inodes;        // the number of inodes in the inodes array
  unsigned int fbv_start_fsblock_num;     // the starting file system block number for the free block vector.
  unsigned int data_start_fsblock_num;
} dfs_superblock;

#define DFS_BLOCKSIZE 1024  // Must be an integer multiple of the disk blocksize
#define DFS_MAX_FILENAME_LENGTH 72
#define DFS_INODESIZE 128

typedef struct dfs_block {
  char data[DFS_BLOCKSIZE];
} dfs_block;

typedef struct dfs_inode {
  // STUDENT: put inode structure internals here
  // IMPORTANT: sizeof(dfs_inode) MUST return 128 in order to fit in enough
  // inodes in the filesystem (and to make your life easier).  To do this, 
  // adjust the maximumm length of the filename until the size of the overall inode 
  // is 128 bytes.
  unsigned char inuse; // 1 bytes
  unsigned char type; // dir or file
  unsigned char permission; // file permission (owner, group, users)
  char ownerid;
  unsigned int num_block;  
  int size; // 4 bytes
  int blockTable [10]; // first 10 virtual blocks, 40 bytes
  int indirectTableBlockNum; // may or may not be allocated, 4 bytes
  char junk[DFS_INODESIZE - 56];
} dfs_inode;

typedef struct block_and_dir_num{
    unsigned int block;
    unsigned int dir_num;
}block_and_dir_num;

typedef struct dir_entry {
  char directory_name[DFS_MAX_FILENAME_LENGTH];
  unsigned int inode_handle;
} dir_entry; 

#define DFS_MAX_FSBLOCK_PER_INODE 266

#define DFS_MAX_DIRECTORY_ENTRIES (DFS_BLOCKSIZE / sizeof(dir_entry))

// one data block for directory inode
typedef struct directory{
  dir_entry entries[DFS_MAX_DIRECTORY_ENTRIES];
  char junk[DFS_BLOCKSIZE - sizeof(dir_entry) * DFS_MAX_DIRECTORY_ENTRIES];
} directory;

#define DFS_MAX_FILESYSTEM_SIZE 0x1000000  // 16MB

#define DFS_FAIL -1
#define DFS_FREE -1
#define DFS_SUCCESS 1

// indicator for type of file to be used in dfs_inode.type
#define DFS_TYPE_DIR 1
#define DFS_TYPE_FILE 2

//Various bits for file permissions

// bit shift to left
#define DFS_OWNER   3
#define DFS_USERS   0

#define DFS_READ		  0x4	//Read permission bit
#define DFS_WRITE		  0x2	//Write permission bit
#define DFS_EXECUTE		0x1	//Execute permission bit

typedef enum boolean
{
  false = 0, 
  true = 1
} bool;

#endif
