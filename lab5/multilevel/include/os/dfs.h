#ifndef __DFS_H__
#define __DFS_H__

#include "dfs_shared.h"

dfs_inode inodes[192]; // all inodes
uint32 fbv[512]; // Free block vector


void DfsModuleInit();
void DfsInvalidate();
int DfsOpenFileSystem();
int DfsCloseFileSystem();
int DfsAllocateBlock();
int DfsFreeBlock(uint32 blocknum);
int DfsReadBlock(uint32 blocknum, dfs_block *b);
int DfsWriteBlock(uint32 blocknum, dfs_block *b);
int DfsInodeOpen();
int DfsInodeDelete(uint32 handle);
int DfsInodeFilenameExists(char *filename);
int DfsInodeReadBytes(uint32 handle, void *mem, int start_byte, int num_bytes);
int DfsInodeWriteBytes(uint32 handle, void *mem, int start_byte, int num_bytes);
int DfsInodeFilesize(uint32 handle);
int DfsInodeAllocateVirtualBlock(uint32 handle, uint32 virtual_blocknum);
int DfsInodeTranslateVirtualToFilesys(uint32 handle, uint32 virtual_blocknum);
int GetBlocksize();


// Added function for multidirectory support
int CheckInode(uint32 handle );
inline int isDir(uint32 inode_handle);
int DfsCheckPermission(int inode_handle, unsigned char mode);
int DfsCheckifDirectoryEntriesAllocated(int dir_num, directory dir, bool allocated );
block_and_dir_num DfsReturnBlkNumAndDirEntry(char* filename, uint32 inode_handle);
block_and_dir_num  DfsCheckAllFsBlockForFilename(char *filename, uint32 inode_handle);
int DfsAssignFilenameAndInodeToDirectoryEntries (char *filename, uint32 fs_block_num, uint32 directory_entry_handle );
int DfsAssignAttributesToInode(uint32 root_inode_handle ,uint32 child_inode_handle, unsigned char type, unsigned char permission);
bool isDirectoryEmpty(uint32 inode_handle);
int DfsDeleteInodeGivenParent(uint32 parent_inode_handle, uint32  leaf_inode_handle, uint32 virtual_block);
void DfsPrintWholeDirectoryBlock(uint32 inode_handle);
int DfsPrintCwdPath();
int DfsReturnChildInode(block_and_dir_num blockdir, uint32 inode_handle);


#endif
