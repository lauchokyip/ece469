#include "usertraps.h"
#include "files_shared.h"

#define COMMAND_MAX_LENGTH 80

static int my_strcmp(char * source, char * target, int n){
  int i;
  
  i = 0;

  //No null character allow
  if(source[i] == 0) return 0;

  while(source[i] != '\0' && i < n){
    if(source[i] != target[i]) {return 0;}
    i++;
  }
  //This is to check if it's fully the same
  if(target[i] != '\0') {return 0;}
  //it's true
  return 1;
}

static void backspace (char **pos) 
{

  /* Back up cursor, overwrite character, back up
      again. */
  Printf("\b \b");
  (*pos)--;
  return;

}

static void my_strcpy(char * source, char * target, int n)
{ 
  int i;

  i = 0;
  while(i < COMMAND_MAX_LENGTH && i < n)
  {
    if(source[i] == 0) break;
    target[i] = source[i];
    i++;
  }
  target[i] = 0;
}


static void read_line(char *line, int size )
{
  char *pos;
  char c;
  char *offset;


  pos = line;
  offset = pos;
  while(1)
  {
      c = GetChar();

      switch (c)
      {

      case '\r':
        *pos = '\0';
        Printf("\n");
        return;

      case 4:
        my_strcpy("exit", line, 5);
        return;

      case 127:
        if(pos - offset <= 0){
           break;
        }
        backspace(&pos);
        break;
      default:
        if (pos < line + size - 1) 
            {
              Printf("%c",c);
              *pos++ = c;
            }
          break;
      
      }
  }
}

/**********************************************************************
 * Given a terminal string input, extract and store the first argument
 * into word and return the number of char in first argument.
**********************************************************************/
int extract_cmd (char* cmd, char* word) {
  int i;
  i = 0;
  while((*cmd != ' ') && (*cmd != '\0')) {
    word[i++] = *cmd;
    cmd++;
  }
  return i;
}

void PrintHelp()
{
  // commands listed in alphabetic order
  Printf("\n");
  Printf("Available command: \n");
  Printf("addline [filename] < [newline] -  add new line to file\n");
  Printf ("cat [filename] -  show the content of the file\n");
  Printf("cd -  change directory\n");
  Printf ("chmod [permission] -  change the permission\n");
  Printf("clear -  clear the whole terminal screen\n");
  Printf("delline [filename] -  delete last line in file\n");
  Printf ("exit -  Exit the shell\n");
  Printf("ls -  list out files and directorys on the current path\n");
  Printf ("mkdir [directory name] -  create a directory\n");
  Printf("pid -  print out the pid of the program\n");
  Printf("pwd -  print out current path of the program\n");
  Printf ("rm [filename] -  delete a file\n");
  Printf ("rmdir [directory name] -  remove a directory\n");
  Printf("source [filename] -  execute file as script\n");
  Printf ("touch [filename] -  create a filename in the directory\n");
  Printf("\n");
}

void bzero (char* buff, unsigned int size) {
  int i;
  for (i = 0; i < size; i++) {
    buff[i] = '\0';
  }
}

void parse_cmd(char* command) {
  int i;
  int j;
  int retval;
  int handle;
  char read_data;
  char arg[COMMAND_MAX_LENGTH];

  if(my_strcmp(command, "addline", 7) == 1) {
      i = 7;
      do {
        read_data = command[++i];
      } while(read_data != '<');

      bzero(arg, sizeof(arg));
      my_strcpy(&(command[8]), arg, i - 9);

      handle = file_open(arg, "w");
      bzero(arg, sizeof(arg));
      my_strcpy(&(command[i+2]), arg, sizeof(arg));

      retval = dstrlen(arg);
      arg[retval] = '\n';
      file_seek(handle, 1, FILE_SEEK_END);

      file_write(handle, arg, retval+1);
      file_close(handle);
    }

    else if(my_strcmp(command, "cat", 3) == 1)
    {
      // has to add a command that check if the file exists in the directory or not

      my_strcpy(&command[4], arg, sizeof(arg));
      retval = file_open(arg, "r");
      if(retval != -1)
      {
        handle = retval;

        retval = file_seek(handle, 0, FILE_SEEK_SET);

        if(retval != -1)
        {
          do
          {
            retval = file_read(handle, (void *)&read_data, 1);
            if(retval != -1) { Printf("%c",read_data); }
          } while (retval != -1 && read_data != 0);
        }
          
       file_close(handle);     
      }
      else
      {
        file_delete(arg);
      }
    }

    else if(my_strcmp(command, "cd", 2) == 1)
    {
      my_strcpy(&command[3], arg, sizeof(arg));
      cddir(arg);
    }

    else if(my_strcmp(command, "chmod", 5) == 1)
    {
      retval = ((command[6] - '0') << 3) | (command[7] - '0');
      my_strcpy(&command[9], arg, sizeof(arg));
      chmod(arg, retval);
    }

    else if(my_strcmp(command, "clear", 5) == 1)
    {
      Printf("\e[1;1H\e[2J");
    }

    else if(my_strcmp(command, "delline", 7) == 1)
    {
      // format: delline [filename]
      bzero(arg, sizeof(arg));
      my_strcpy(&(command[8]), arg, sizeof(arg));
      handle = file_open(arg, "rw");

      if(file_seek(handle, -79, FILE_SEEK_END) != FILE_SUCCESS) {
        file_seek(handle, 0, FILE_SEEK_SET);
      }

      i = file_read(handle, arg, sizeof(arg)) - 1;

      j = i;
      
      do {
        read_data = arg[--j];
      } while ((j >= 0) && (read_data != '\n'));

      file_seek(handle, j - i + 1, FILE_SEEK_END);

      bzero(arg, sizeof(arg));
      file_write(handle, arg, 1 - (j- i));
      file_close(handle);
    }

    else if(my_strcmp(command, "help", 4) == 1) 
    {
      PrintHelp();
    }

    else if(my_strcmp(command, "ls", 2) == 1) 
    {
      lsdir();
    }

    else if(my_strcmp(command, "mkdir", 5) == 1) 
    {
      my_strcpy(&command[6], arg, sizeof(arg));
      mkdir(arg, 7 << 3);
    }

    else if(my_strcmp(command, "pid", 3) == 1) 
    {
      Printf("%d\n", getpid());
    }

    else if(my_strcmp(command, "pwd", 3) == 1) 
    {
      printcwd();
      Printf("\n");
    }

    else if(my_strcmp(command, "rmdir", 5) == 1) 
    {
      my_strcpy(&command[6], arg, sizeof(arg));
      rmdir(arg);
    }

    else if(my_strcmp(command, "rm", 2) == 1)
    {
      my_strcpy(&command[3], arg, sizeof(arg));
      rm(arg);
    }

    else if(my_strcmp(command, "source", 6) == 1)
    {
      my_strcpy(&command[7], arg, sizeof(arg));
      handle = file_open(arg, "r");

      if(handle == FILE_FAIL) {
        Printf("Cannot open file\n");
      }

      else {
        file_seek(handle, 0, FILE_SEEK_SET);

        do {
          i = 0;
          bzero(command, COMMAND_MAX_LENGTH);

          do {
            retval = file_read(handle, &read_data, 1);
            if(read_data != '\n') command[i++] = read_data;
          } while(read_data != '\n');

          if(retval != FILE_EOF) parse_cmd(command);

        } while(retval != FILE_EOF);

        file_close(handle);
      }
    }

    else if(my_strcmp(command, "touch", 5) == 1)
    {
      my_strcpy(&command[6], arg, sizeof(arg));
      handle = file_open(arg,"rw");
      file_close(handle);
    }

    else 
    {
      Printf("%s: command not found\n", command);
    }
}

int dstrlen (const char *s)
{
  int		i = 0;

  while (*(s++) != '\0') {
    i++;
  }
  return (i);
}

void main (int argc, char *argv[])
{
  int i;
  int j;
  int retval;
	int handle;
	char read_data;
  char data[512];
  char *file1 = "a/b/c/test_file";
  int test = 0; // this is to write stuff into a file for testing

  char command[COMMAND_MAX_LENGTH];

  // run_os_tests();

  if(test == 1)
  {
    retval = file_open(file1, "rw");
    Printf("called file_open, return %d\n", retval);
    handle = retval;
    for (i=0; i< 511; i++) 
    {
      data[i] = 'A';
    }
    data[511] = '\n';
    retval = file_write(handle, (void *)data, 512);
    Printf("called file_write, return %d\n", retval);
    retval = file_close(handle);
    Printf("called file_close, return %d\n", retval);

    Exit();
  }

  Printf("Starting shell\n");
  Printf("Type help for available commands\n");
  while(1)
  {
    Printf("lau:");
    printcwd();
    Printf(" ");

    read_line(command, sizeof(command));

    if(my_strcmp(command, "exit", 4) == 1) {
      break;
    }
    else {
      parse_cmd(command);
    }
  }

  Printf("\n");
  Exit();
}