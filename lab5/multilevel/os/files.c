#include "ostraps.h"
#include "dlxos.h"
#include "process.h"
#include "dfs.h"
#include "files.h"
#include "synch.h"

// referenced from files.c
file_descriptor files_arr[FILE_MAX_FILE_DESCRIPTOR];
lock_t file_descriptor_lock;
static int isInit = 0;

// added for multi directory support
directory dir_buffer;




////////////////////////////////////////////////////////////
//
//  Self-written string compare function
//
//
////////////////////////////////////////////////////////////

static int my_strcmp(char * source, char * target){
  int i;
  
  i = 0;

  //No null character allow
  if(source[i] == 0) return 0;

  while(source[i] != '\0' && i < FILE_MAX_FILENAME_LENGTH){
    if(source[i] != target[i]) {return 0;}
    i++;
  }
  //This is to check if it's fully the same
  if(target[i] != '\0') {return 0;}
  //it's true
  return 1;
}

////////////////////////////////////////////////////////////
//
//  Check file_handle
//
//
////////////////////////////////////////////////////////////

static int check_handle(int handle){

    
    if(handle < 0){
        printf("FATAL ERROR: file handle too small in FileClose\n");
        return FILE_FAIL;
    }
    if(handle > FILE_MAX_FILE_DESCRIPTOR) {
        printf("FATAL ERROR: file handle too large in FileClose\n");
        return FILE_FAIL;
    }
    if(files_arr[handle].mode == 0){    
        printf("File array is not allocated\n");
        return FILE_FAIL;
    }


    return FILE_SUCCESS;
}


////////////////////////////////////////////////////////////
//
//  Initialize files_arr
//
//
////////////////////////////////////////////////////////////
static void init_check() {
    int i;
    
    
    if(isInit == 1) 
    {
        return;
    }

    LockHandleAcquire(file_descriptor_lock);
    for(i = 0; i < FILE_MAX_FILE_DESCRIPTOR; i++) {
        files_arr[i].mode = 0;
        files_arr[i].eof_flag = 0;
        files_arr[i].pid = -1;
        files_arr[i].inode_handle = -1;
        files_arr[i].cur_pos = 0;
    }
    LockHandleRelease(file_descriptor_lock);

    isInit = 1;
    
    
}

////////////////////////////////////////////////////////////
//
//  Convert string to byte mode
//
//
////////////////////////////////////////////////////////////
static char mode_str_to_mode_byte(char *mode)
{
    

    if(dstrlen(mode) > 2 ){
        printf("The mode should not have more than 3 characters\n");
        return (char)FILE_FAIL;
    }

    if(my_strcmp(mode, "r") == 1){
        
        return (char)FILE_MODE_READ;
    }

    if(my_strcmp(mode, "w") == 1){
        
        return (char)FILE_MODE_WRITE;
    }

    if(my_strcmp(mode, "rw") == 1){
        
        return (char)FILE_MODE_RW;
    }

    printf("Mode does not contain r or w or rw\n");
    return (char)FILE_FAIL;
}


////////////////////////////////////////////////////////////
//
//  This is to get the free file description array handle
//
//
////////////////////////////////////////////////////////////
static int get_file_descript_handle() {
    int i;
    
    

    LockHandleAcquire(file_descriptor_lock);

    for (i = 0; i < FILE_MAX_FILE_DESCRIPTOR; i++){
        if(files_arr[i].mode == 0) {
            LockHandleRelease(file_descriptor_lock);
            return i;
        }
    }
    
    LockHandleRelease(file_descriptor_lock);
    printf("FATAL ERROR: Unable to find free file descriptor in helper function!\n");
    return FILE_FAIL;
}

////////////////////////////////////////////////////////////
//
//  free the file_description_array given the array handle
//
//
////////////////////////////////////////////////////////////
static int free_file_descript_handle(unsigned int handle)
{
    

    LockHandleAcquire(file_descriptor_lock);
    
    if(files_arr[handle].mode == 0) {
        printf("Error: Attempting to free already unallocated file descriptor\n");
        LockHandleRelease(file_descriptor_lock);
        return FILE_FAIL;
    }
    files_arr[handle].eof_flag = 0;
    files_arr[handle].mode = 0;
    files_arr[handle].pid = -1;
    files_arr[handle].inode_handle = -1;
    files_arr[handle].cur_pos = 0;

    
    LockHandleRelease(file_descriptor_lock);
    return FILE_SUCCESS;
}



////////////////////////////////////////////////////////////
//
//  Find the file description, return FAIL if file is not found
//
//
////////////////////////////////////////////////////////////
static int FileDescriptorExists(uint32 inode_handle) {
    
    int i; //loop variable
    int pid;


    pid = GetCurrentPid();

    if (CheckInode(inode_handle) == DFS_FAIL) {
        printf("Inode handle %d does not exist in FileDescriptorExists\n", inode_handle);
        return FILE_FAIL;
    }
 
    LockHandleAcquire(file_descriptor_lock);


    for(i = 0; i < FILE_MAX_FILE_DESCRIPTOR; i++)
    {
        if(files_arr[i].inode_handle == inode_handle) 
        {
            if(files_arr[i].pid == pid) 
            {
                LockHandleRelease(file_descriptor_lock);
                return i;
            }
            else 
            {
                LockHandleRelease(file_descriptor_lock);
                return -2;
            }
        }
        i++;
    }

    
    LockHandleRelease(file_descriptor_lock);
    
    
    return FILE_FAIL;
}

////////////////////////////////////////////////////////////
//
//  Check if the path is valid
//
//  return -1 if the path is invalid return 1 if valid
////////////////////////////////////////////////////////////
int check_path_condition
(char* path, char* filename)
{
    int i;
    int j;
    
    if(path == NULL) 
    {
        printf("FATAL ERROR: Invalid path received in my_filename_cpy\n");
        return -1;
    }
    if(filename == NULL) 
    {
        printf("FATAL ERROR: Invalid buffer received in my_filename_cpy\n");
        return -1;
    }
    
    j = 0;
    // Here is to check if the whole path has double backslash and backslash at the end
    for (i = 0; i < dstrlen(path); i++){
        
        if(path[i] == '/'){
            
            filename[j] = 0;
            if(path[i+1] == '/')
            {   
                printf("double backslash detected when checking path\n");
                return -1;
            }
            if(dstrlen(filename) > 72)
            {
                printf("Path/filename is too long \n");
                return -1; 
            }
            j = 0;
        }
        
        filename[j] = path[i];
        j++;
        
    }
    if(path [i-1] == '/'){
        printf("Reached end of file path. There exists a '/' at the end of filepath.\n");
        return -1;
    }
    return 1;
}

////////////////////////////////////////////////////////////
//
//  copy from the path name to filename
//
//  return string to filename and next file path index to start point
//
//  Example: usage
//   
//        char path[] = "/a/b/chaskjlkhlkjhjjjashkdlhakjdhkalsdhlkjahsdkjahsd/d/gkljhjhjkhkjlalkjahdlkjhaksljhdlkjahsldk";
//        char filename[72];
//        int startPoint;
//
//        if(check_filename_condition(path,filename) == -1) return 1;
//
//       startPoint = my_filename_cpy(path,filename,0);
//       while(startPoint > 0){
//            printf("%d ",startPoint );
//            printf("%s \n", filename);
//            startPoint = my_filename_cpy(path,filename,startPoint);
//        }
//      
//  Returns:
//        2 a                                                                                                                                                                                          
//        4 b                                                                                                                                                                                          
//        52 chaskjlkhlkjhjjjashkdlhakjdhkalsdhlkjahsdkjahsd                                                                                                                                           
//        54 d                                                                                                                                                                                         
//        95 gkljhjhjkhkjlalkjahdlkjhaksljhdlkjahsldk                                                                                                                                                  
//        Reach end of path string        
//      
//
////////////////////////////////////////////////////////////
static int my_filename_cpy
(char* path, char* filename, int startPoint) 
{

    int i;
    int j;
    
    i = startPoint;
    j = 0;
    
    if(path[i] == 0) 
    {
        printf("Reach end of path string \n");
        return -1;
    }
    if(path[i] == '/') i++;

    while((path[i] != '/') && (path[i] != '\0'))  {
        
        if(i == (dstrlen(path)) ){
            break;     
        }
        
        filename[j] = path[i];
        i++;
        j++;
        
    }

    filename[j] = 0;
    return i; 
}



////////////////////////////////////////////////////////////
//
//  Check if the user has the permission to recursively go 
//  down a path
//
//  return the inode handle of the file if the path is invalid 
//  return -1 if valid
////////////////////////////////////////////////////////////
static int CheckPathPermission(char * path, char *filename, uint32 inode_handle)
{
    unsigned int nextStartPoint;
    unsigned int pathLen;
    block_and_dir_num blockdir;
  
    init_check();
    pathLen = dstrlen(path);
    
    
    if(check_path_condition(path,filename) == -1) return FILE_FAIL;

    nextStartPoint = my_filename_cpy(path,filename,0);
    while((nextStartPoint > 0) && (nextStartPoint < pathLen - 1))
    {
        if(DfsCheckPermission(inode_handle, DFS_EXECUTE) == false) 
        {
            printf("FATAL ERROR: User has no execute permission for directory inode handle %d\n", inode_handle);
            return FILE_FAIL;
        }
        
        // it's either an error or cannot found the path
        blockdir = DfsCheckAllFsBlockForFilename(filename, inode_handle);
        if(blockdir.dir_num == DFS_FAIL) 
        {
            printf("FATAL ERROR: Cannot obtain inode of directory in CheckPathPermission\n");
            return FILE_FAIL;
        }
        if(blockdir.dir_num == -2) {
            printf("FATAL ERROR: Cannot obtain inode of directory in CheckPathPermission\n");
            return FILE_FAIL;
        }

        // check if inode is dir
        if(inodes[inode_handle].type != DFS_TYPE_DIR) 
        {
            printf("FATAL ERROR: File being visited is not a directory in CheckPathPermission\n");
            return FILE_FAIL;
        }
        
    
        // read the block to the directory handle
        if (DfsReadBlock(inodes[inode_handle].blockTable[blockdir.block], (dfs_block*) &dir_buffer) == DFS_FAIL)
        {
            printf("FATAL ERROR: Unable to read directory struct block in CheckPathPermission\n");
            return FILE_FAIL;
        }
        inode_handle = dir_buffer.entries[blockdir.dir_num].inode_handle;
        
        
        
        nextStartPoint = my_filename_cpy(path, filename, nextStartPoint);
    }
    
    
    
    return inode_handle;
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Open the given filename with one of three possible modes: "r", "w", or "rw". 
// return FILE_FAIL on failure (e.g., when a process tries to open a file that is already open for another process),
// and the handle of a file descriptor on success. Remember to use locks whenever you allocate a new file descriptor.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
int FileOpen(char *path, char *mode)
{
    int what_mode;
    unsigned int file_handle;
    char filename[DFS_MAX_FILENAME_LENGTH];
    block_and_dir_num blockdir;
    uint32 inode_handle;
    uint32 child_inode_handle;
    int fs_blocknum;
    uint32 start_cwd;

    
    
    init_check();


    if(path[0] == '/') {
        start_cwd = 0;
    }
    else {
        start_cwd = GetCwd();
    }

    inode_handle = CheckPathPermission(path, filename, start_cwd);
    
    if(inode_handle == FILE_FAIL)
    {
        printf("Cannnot go down the path! \n");
        return FILE_FAIL;
    }

    
    
    ////////////////////////////////////////////////

    // check if the file exists or not in the directory
    blockdir = DfsCheckAllFsBlockForFilename(filename, inode_handle);
    if(blockdir.dir_num == -2) 
    {
        printf("FATAL ERROR: when parsing in filename and inode handle\n");
        return FILE_FAIL;
    }

    // the file does not exists
    if(blockdir.dir_num == DFS_FAIL) 
    {
        blockdir = DfsReturnBlkNumAndDirEntry(filename,inode_handle);
        if (blockdir.dir_num == DFS_FAIL) 
        {
            printf("FATAL ERROR: Unable to find available blocknum and entry for %s\n", filename);
            return FILE_FAIL;
        }

        fs_blocknum = DfsInodeTranslateVirtualToFilesys(inode_handle, blockdir.block);

        child_inode_handle = DfsAssignFilenameAndInodeToDirectoryEntries(filename, fs_blocknum, blockdir.dir_num);
        
        if(child_inode_handle == DFS_FAIL) 
        {
            printf("FATAL ERROR: Unable to assign filename and inode number to a directory entry\n");
            return FILE_FAIL;
        }
        
        if(DfsAssignAttributesToInode(inode_handle, child_inode_handle, DFS_TYPE_FILE, (DFS_READ | DFS_WRITE | DFS_EXECUTE) << DFS_OWNER) == DFS_FAIL)
        {
            printf("FATAL ERROR: Unable to assign attributes to inode\n");
            return FILE_FAIL;
        }
    }

    else
    {
        // file exists so get the child inode handle
        child_inode_handle = DfsReturnChildInode(blockdir, inode_handle);
    }
    
    // check if user has permissions to open file in specified mode
    what_mode = (int) mode_str_to_mode_byte(mode);

    switch (what_mode) 
    {
        case FILE_MODE_READ: 

            if(DfsCheckPermission(child_inode_handle, DFS_READ) == false)
            {
                printf("FATAL ERROR: User does not have read permission\n");
                return FILE_FAIL;
            }
            break;
        case FILE_MODE_WRITE:
            if(DfsCheckPermission(child_inode_handle, DFS_WRITE) == false)
            {
                printf("FATAL ERROR: User does not have write permission\n");
                return FILE_FAIL;
            }
            break;
        case FILE_MODE_RW:
            if(DfsCheckPermission(child_inode_handle, (DFS_WRITE | DFS_READ) ) == false)
            {
                printf("FATAL ERROR: User does not have write and read permission\n");
                return FILE_FAIL;
            }
            break;
        
        default:
            printf("Fatal Erorr: Mode cannot interpreted \n");
            return FILE_FAIL;
    }

    // search if file descriptor for the inode has been previously allocated
    file_handle = FileDescriptorExists(child_inode_handle);

    
    // return fail if file descriptor opened by another process or filename invalid
    if(file_handle == -2) {
        printf("FATAL ERROR: Cannot get file handle in FileOpen\n");
        return DFS_FAIL;
    }
    // return handle if file descriptor opened by same process
    
    if(file_handle != FILE_FAIL) {
        return file_handle;
    }


    // Get free file_handle
    file_handle =  get_file_descript_handle();
    if(file_handle == FILE_FAIL) {
        printf("FATAL ERROR: Cannot get available file descriptor in FileOpen\n");
        return FILE_FAIL;
    }
    
    
    // check what mode the file has
    if(what_mode == FILE_FAIL) {
        printf("FATAL ERROR: Unrecognized mode in FileOpen\n");
        return FILE_FAIL;
    }

    LockHandleAcquire(file_descriptor_lock);
    // initilaize inode handle


    files_arr[file_handle].inode_handle = child_inode_handle;

    // initialize mode
    files_arr[file_handle].mode = what_mode;
    // assign pid to each file array
    files_arr[file_handle].pid = GetCurrentPid();
    files_arr[file_handle].cur_pos = 0;
    LockHandleRelease(file_descriptor_lock);

    return file_handle;
}

// Close the given file descriptor handle. Return FILE_FAIL on failure, and FILE_SUCCESS on success
int FileClose(int handle)
{
    
    //Check if handle is valid
    if(check_handle(handle) == FILE_FAIL) {
        printf("FATAL ERROR: Invalid handle in FileClose\n");
        return FILE_FAIL;
    }
    
    //Check if the file is opened
    LockHandleAcquire(file_descriptor_lock);
    if(files_arr[handle].mode == 0) {
        
        LockHandleRelease(file_descriptor_lock);
        return FILE_FAIL;
    }

    //Check if same process is closing the file
    if(files_arr[handle].pid != GetCurrentPid()) {
        
        LockHandleRelease(file_descriptor_lock);
        return FILE_FAIL;
    }

    // Reset the files_array
    if(free_file_descript_handle(handle) == FILE_FAIL) {
        printf("FATAL ERROR: Unable to free file descriptor %d\n", handle);
        return FILE_FAIL;
    }
   
    LockHandleRelease(file_descriptor_lock);
    return FILE_SUCCESS;
}


// Read num_bytes from the open file descriptor identified by handle. 
// Return FILE_FAIL on failure or upon reaching end of file, and the number of bytes read on success. 
// If end of file is reached, the end-of-file flag in the file descriptor should be set.
int FileRead(int file_handle, void *mem, int num_bytes)
{
    int inode_handle;
    
    inode_handle = files_arr[file_handle].inode_handle;

    //Check if inode is non-dir
    if(inodes[inode_handle].type != DFS_TYPE_FILE) {
        printf("FATAL ERROR: Attempting to read a directory in FileRead\n");
        return FILE_FAIL;
    }
    
    //Check if handle is valid
    if(check_handle(file_handle) == FILE_FAIL) {
        printf("FATAL ERROR: Invalid handle in FileRead\n");
        return FILE_FAIL;
    }
    //check if num_bytes is more than allowed bytes
    if(num_bytes > FILE_MAX_READWRITE_BYTES) {
        printf("FATAL ERROR: Number of bytes to read exceeds limit\n");
        return FILE_FAIL;
    }

    LockHandleAcquire(file_descriptor_lock);

    // Check if the file is in read mode 
    if((files_arr[file_handle].mode & FILE_MODE_READ) != FILE_MODE_READ) {
        printf("FATAL ERROR: The file does not have read access\n");
        LockHandleRelease(file_descriptor_lock);
        return FILE_FAIL;
    }
    //Check if EOF bit is set
    if(files_arr[file_handle].eof_flag == 1) {
        // printf("File descriptor has reached end of file in FileRead\n");
        LockHandleRelease(file_descriptor_lock);
        return FILE_EOF;
    }
    
    // if remaining unread bytes is less than number of bytes, only read remaining bytes
    if((inodes[inode_handle].size - files_arr[file_handle].cur_pos) < num_bytes) {
        num_bytes = inodes[inode_handle].size - files_arr[file_handle].cur_pos;
    }

    if(DfsInodeReadBytes(inode_handle, mem, files_arr[file_handle].cur_pos, num_bytes) == num_bytes) {
        FileSeek(file_handle, num_bytes, FILE_SEEK_CUR);
        // set eof flag upon reaching end of file
        if(files_arr[file_handle].cur_pos == inodes[inode_handle].size) {
            files_arr[file_handle].eof_flag = 1;
        }
        LockHandleRelease(file_descriptor_lock);
        return num_bytes;
    }

    printf("FATAL ERROR: Unable to read bytes from file\n");
    LockHandleRelease(file_descriptor_lock);

    return FILE_FAIL;
}

// Write num_bytes to the open file descriptor identified by handle. 
// Return FILE_FAIL on failure, and the number of bytes written on success.
int FileWrite(int file_handle, void *mem, int num_bytes)
{
    int inode_handle;
    int fs_blocksize;



    inode_handle = files_arr[file_handle].inode_handle;

    //Check if inode is non-dir
    if(inodes[inode_handle].type != DFS_TYPE_FILE) {
        printf("FATAL ERROR: Attempting to read a directory in FileWrite\n");
        return FILE_FAIL;
    }
    
    //Check if handle is valid
    if(check_handle(file_handle) == FILE_FAIL) {
        printf("FATAL ERROR: Invalid handle in FileWrite\n");
        return FILE_FAIL;
    }
    //check if num_bytes is more than allowed bytes
    if(num_bytes > FILE_MAX_READWRITE_BYTES) {
        printf("FATAL ERROR: Number of bytes to read exceeds limit\n");
        return FILE_FAIL;
    }

    fs_blocksize = GetBlocksize();

    LockHandleAcquire(file_descriptor_lock);

    // Check if the file is in write mode 
    if((files_arr[file_handle].mode & FILE_MODE_WRITE) != FILE_MODE_WRITE) {
        printf("FATAL ERROR: The file does not have write access\n");
        LockHandleRelease(file_descriptor_lock);
        return FILE_FAIL;
    }

    // get the inode handle for the file
    inode_handle = files_arr[file_handle].inode_handle;

    // if remaining space available in file is less than num byte to write, return fail
    if((fs_blocksize * (10 + fs_blocksize / 4) - inodes[inode_handle].size) < num_bytes) {
        printf("FATAL ERROR: Attempting to write more bytes than available bytes in file\n");
        LockHandleRelease(file_descriptor_lock);
        return FILE_FAIL;
    }

    if(DfsInodeWriteBytes(inode_handle, mem, files_arr[file_handle].cur_pos, num_bytes) == num_bytes) {
        FileSeek(file_handle, num_bytes, FILE_SEEK_CUR);
        
        LockHandleRelease(file_descriptor_lock);
        return num_bytes;
    }

    printf("FATAL ERROR: Unable to write bytes into file\n");
    LockHandleRelease(file_descriptor_lock);

    return FILE_FAIL;
}

// Seek num_bytes within the file descriptor identified by handle, 
// from the location specified by from_where. There are three possible values for from_where:
// FILE_SEEK_CUR (seek relative to the current position), FILE_SEEK_SET (seek relative to the beginning of the file), 
// and FILE_SEEK_END (seek relative to the end of the file). Any seek operation will clear the eof flag.
int FileSeek(int handle, int num_bytes, int from_where)
{
    int inode_handle;
    int check;

    
    //Check if handle is valid
    if(check_handle(handle) == FILE_FAIL) {
        printf("FATAL ERROR: Invalid handle in FileSeek\n");
        return FILE_FAIL;
    }

    LockHandleAcquire(file_descriptor_lock);
    inode_handle = files_arr[handle].inode_handle;

    switch (from_where)
    {
        case FILE_SEEK_SET:
            check = num_bytes;
            break;
        case FILE_SEEK_CUR:
            check = files_arr[handle].cur_pos + num_bytes;
            break;
        case FILE_SEEK_END:
            check = inodes[inode_handle].size - 1 + num_bytes;
            break;
        default:
            printf("The Mode Provided is not valid! \n");
            LockHandleRelease(file_descriptor_lock);
            return FILE_FAIL;
    }
    if((check >= 0) && (check <= inodes[inode_handle].size)) {
        files_arr[handle].eof_flag = 0;
        files_arr[handle].cur_pos = check;
        LockHandleRelease(file_descriptor_lock);
        return FILE_SUCCESS;
    }

    LockHandleRelease(file_descriptor_lock);
    // printf("FATAL ERROR: Unable to seek file position %d\n", check);

    return FILE_FAIL;
}

// // Delete the file specified by filename. 
// // Return FILE_FAIL on failure, and FILE_SUCCESS on success.

int FileDelete(char *path, int type)
{
    block_and_dir_num blockdir;
    char filename[DFS_MAX_FILENAME_LENGTH];
    uint32 inode_handle;
    uint32 child_inode_handle;
    uint32 start_cwd;
    uint32 file_handle;
    int num_block;
    int fs_block;
    int i;

    

    if(path[0] == '/') {
        start_cwd = 0;
    }
    else {
        start_cwd = GetCwd();
    }

    if(my_strcmp(path, ".") == 1) {
        printf("Error in Rmdir: Attempting to delete cwd\n");
    }

    inode_handle = CheckPathPermission(path, filename, start_cwd);

    if(type == DFS_TYPE_FILE) {
        //Check the filename if it exits on file array
        file_handle = FileDescriptorExists(inode_handle);
        // return fail if name is invalid
        if(file_handle == -2) {
            printf("FATAL ERROR: Invalid filename received in FileDelete\n");
            return FILE_FAIL;
        }
        // if file descriptor exists, deallocate it
        if(file_handle != FILE_FAIL) {
            if(FileClose(file_handle) == FILE_FAIL) {
                printf("FATAL ERROR: Cannot close an allocated file with handle %d",file_handle);
                return FILE_FAIL;
            }
        }
    }
    
    if(inode_handle == FILE_FAIL)
    {
        printf("FATAL ERROR: User does not have path permission in FileDelete\n");
        return FILE_FAIL;
    }
    blockdir = DfsCheckAllFsBlockForFilename(filename, inode_handle);
    if(blockdir.dir_num == DFS_FAIL) 
    {
        printf("FATAL ERROR: Cannot find filename %s in inode %d in FileDelete\n", filename, inode_handle);
        return FILE_FAIL;
    }
    if(blockdir.dir_num == -2) {
        printf("FATAL ERROR: Cannot obtain inode of directory in FileDelete\n");
        return FILE_FAIL;
    }
    DfsReadBlock(inodes[inode_handle].blockTable[blockdir.block], (dfs_block*) &dir_buffer);

    child_inode_handle = dir_buffer.entries[blockdir.dir_num].inode_handle;
    
    // if(inodes[child_inode_handle].num_block != 0) {
    //     printf("FATAL ERROR: Attempting to rmdir a non-empty directory (%d blocks)\n", inodes[child_inode_handle].num_block);
    //     return FILE_FAIL;
    // }

    if(inodes[child_inode_handle].type != type) 
    {
        printf("FATAL ERROR: Rmdir should be for dir, rm should be for file in FileDelete\n");
        return FILE_FAIL;
    }

    if(type == DFS_TYPE_DIR) {
        num_block = inodes[child_inode_handle].num_block;
        for(i = 0 ; i < DFS_MAX_FSBLOCK_PER_INODE; i++)
        {
            // no block left is allocated, return
            if(num_block == 0) break;

            fs_block = DfsInodeTranslateVirtualToFilesys(child_inode_handle,i);
            // if the virtual block is allocated
            if(fs_block != DFS_FAIL)
            {
                // check if the corresponding directory is empty
                if(DfsReadBlock(fs_block, (dfs_block *) &dir_buffer) == DFS_FAIL)
                {
                    printf("Disk Read error\n");
                    return FILE_FAIL;
                }
                if(DfsCheckifDirectoryEntriesAllocated(i, dir_buffer, true) != DFS_FREE )
                {
                    printf("FATAL ERROR: Unable to delete non-empty directory in FileDelete\n");
                    return FILE_FAIL;
                }
                //decrease num_block because we have found one
                num_block--;
            }

        }
    }
    
    if (DfsDeleteInodeGivenParent(inode_handle, child_inode_handle, blockdir.block) == DFS_FAIL)
    {
        printf("FATAL ERROR: Unable to delete directory in FileDelete\n");
        return FILE_FAIL;
    }
    
    

    return FILE_SUCCESS;
}



////////////////////////////////////////////////////////////
//
//  Create a new directory
//
//  return FILE_FAIL if error occured else FILE_SUCCESS
////////////////////////////////////////////////////////////
int MkDir (char* filename, int permissions) {
    block_and_dir_num blockdir;
    uint32 inode_handle;
    uint32 child_inode_handle;
    int fs_blocknum;
    int i;
    
    

    i = 0;
    while(i < DFS_MAX_FILENAME_LENGTH) {
        if(filename[i++] == '/') {
            printf("FATAL ERROR: Filename with / character in Mkdir\n");
            return FILE_FAIL;
        }
    }

    inode_handle = GetCwd();
    if(DfsCheckPermission(inode_handle, DFS_WRITE) == false) {
        printf("Write permission needed to create a file or directory\n");
        return FILE_FAIL;
    }

     // check if the file exists or not in the directory
    blockdir = DfsCheckAllFsBlockForFilename(filename, inode_handle);

    if(blockdir.dir_num == -2) 
    {
        printf("FATAL ERROR: when parsing in filename and inode handle\n");
        return FILE_FAIL;
    }
    if(blockdir.dir_num == DFS_FAIL) 
    {
        blockdir = DfsReturnBlkNumAndDirEntry(filename, inode_handle);

        if (blockdir.dir_num == DFS_FAIL) 
        {
            printf("FATAL ERROR: Unable to find available blocknum and entry for %s\n", filename);
            return FILE_FAIL;
        }

        fs_blocknum = DfsInodeTranslateVirtualToFilesys(inode_handle, blockdir.block);

        child_inode_handle = DfsAssignFilenameAndInodeToDirectoryEntries(filename, fs_blocknum, blockdir.dir_num);

        if(child_inode_handle == DFS_FAIL) 
        {
            printf("FATAL ERROR: Unable to assign filename and inode number to a directory entry\n");
            return FILE_FAIL;
        }
        
        if(DfsAssignAttributesToInode(inode_handle, child_inode_handle, DFS_TYPE_DIR, permissions) == DFS_FAIL)
        {
            printf("FATAL ERROR: Unable to assign attributes to inode\n");
            return FILE_FAIL;
        }

        return FILE_SUCCESS;
    }

    // directory or file with same name exists, cannot create
    printf("FATAL ERROR: A file or directory with same name already exists\n");
    
    return FILE_FAIL;
}

////////////////////////////////////////////////////////////
//
//  Create a new directory
//
//
//  return FILE_FAIL if error occured else FILE_SUCCESS
////////////////////////////////////////////////////////////
int Cd (char* path) {
    block_and_dir_num blockdir;
    char filename[DFS_MAX_FILENAME_LENGTH];
    uint32 inode_handle;
    uint32 child_inode_handle;
    uint32 start_cwd;

    

    if(path[0] == '/') {
        start_cwd = 0;
    }
    else {
        start_cwd = GetCwd();
    }

    inode_handle = CheckPathPermission(path, filename, start_cwd);
    
    if(inode_handle == FILE_FAIL) 
    {
        printf("FATAL ERROR: User does not have path permission in Cd\n");
        return FILE_FAIL;
    }
    blockdir = DfsCheckAllFsBlockForFilename(filename, inode_handle);
    if(blockdir.dir_num == DFS_FAIL) 
    {
        printf("FATAL ERROR: Cannot obtain inode of directory in Cd\n");
        return FILE_FAIL;
    }
    if(blockdir.dir_num == -2) {
        printf("FATAL ERROR: Cannot obtain inode of directory in Cd\n");
        return FILE_FAIL;
    }
    DfsReadBlock(inodes[inode_handle].blockTable[blockdir.block], (dfs_block*) &dir_buffer);

    child_inode_handle = dir_buffer.entries[blockdir.dir_num].inode_handle;
    if(inodes[child_inode_handle].type != DFS_TYPE_DIR) 
    {
        printf("FATAL ERROR: Attempting to cd into a non-directory file\n");
        return FILE_FAIL;
    }
    if(DfsCheckPermission(child_inode_handle, DFS_EXECUTE) == false) {
        printf("Execute permission needed to cd into directory\n");
        return FILE_FAIL;
    }
    SetCwd(child_inode_handle);

    

    return FILE_SUCCESS;
}


////////////////////////////////////////////////////////////
//
//  list the current directory 
//
//
//  
////////////////////////////////////////////////////////////
void ListDirectory()
{
    DfsPrintWholeDirectoryBlock(GetCwd());
}

// this is taking the loop iteration approach rather than recursive call
// (which is more reliable but may cause function to exceed page limit)
int FilePrintCwdPath() 
{
    if (DfsPrintCwdPath() == DFS_FAIL) {
        printf("FATAL ERROR: Unable to print cwd path\n");
        return FILE_FAIL;
    }
    return FILE_SUCCESS;
}

int FileChmod(char* path, int permission) {
    block_and_dir_num blockdir;
    char filename[DFS_MAX_FILENAME_LENGTH];
    uint32 inode_handle;
    uint32 child_inode_handle;
    uint32 start_cwd;

    

    if(path[0] == '/') {
        start_cwd = 0;
    }
    else {
        start_cwd = GetCwd();
    }

    inode_handle = CheckPathPermission(path, filename, start_cwd);
    
    if(inode_handle == FILE_FAIL) 
    {
        printf("FATAL ERROR: User does not have path permission in Chmod\n");
        return FILE_FAIL;
    }
    blockdir = DfsCheckAllFsBlockForFilename(filename, inode_handle);
    if(blockdir.dir_num == DFS_FAIL) 
    {
        printf("FATAL ERROR: Cannot obtain inode of directory in Chmod\n");
        return FILE_FAIL;
    }
    if(blockdir.dir_num == -2) {
        printf("FATAL ERROR: Cannot obtain inode of directory in Chmod\n");
        return FILE_FAIL;
    }
    
    DfsReadBlock(inodes[inode_handle].blockTable[blockdir.block], (dfs_block*) &dir_buffer);

    child_inode_handle = dir_buffer.entries[blockdir.dir_num].inode_handle;
    inodes[child_inode_handle].permission = permission;

    

    return FILE_SUCCESS;
}