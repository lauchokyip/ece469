#include "ostraps.h"
#include "dlxos.h"
#include "traps.h"
#include "disk.h"
#include "dfs.h"
#include "process.h"


typedef struct root_child{
  int root_inode[5];
  int child_inode[5];

} root_child;

void RunOSTests() {
  
  // // STUDENT: run any os-level tests here
  // int i;
  // // int j;
  // // directory dir_buffer;
  // // dfs_block dfs_buffer;
  // block_and_dir_num test;
  // char test_char = 'a';
  // char test_filename[DFS_MAX_FILENAME_LENGTH];
  // int inode_handle;
  // int root_inode_handle;
  // int fs_block;
  // root_child test_root_child;
  // int check;

  // printf("Testing...\n");

  // bzero(test_filename, sizeof(test_filename));

  // // test 0: test directory
  // // root_inode_handle = 0;

  // // for(i = 0 ; i < 5; i++){
  // test_filename[0] = test_char;

  // //   // get free virtual block num and directory entry
  // //   test = DfsReturnBlkNumAndDirEntry(test_filename);

  // //   if(test.dir_num == DFS_FAIL) 
  // //   {
  // //     printf("FATAL ERROR: Unable to find available blocknum and entry for %c\n", test_char);
  // //     GracefulExit();
  // //   }



  // //   // obtain the filesystem block num for the virtual block in cwd
  // //   fs_block = DfsInodeTranslateVirtualToFilesys(root_inode_handle, test.block);



  // //   // create inode for the new created directory
  // //   inode_handle = DfsAssignFilenameAndInodeToDirectoryEntries(test_filename, fs_block, test.dir_num);
  // //   if(inode_handle == DFS_FAIL) {
  // //     printf("FATAL ERROR: Unable to assign filename and inode number to a directory entry\n");
  // //     GracefulExit();
  // //   }



  // //   // Change the attributes for new directory
  // //   if(DfsAssignAttributesToInode(root_inode_handle, inode_handle, DFS_TYPE_DIR, (DFS_READ | DFS_WRITE | DFS_EXECUTE) << DFS_OWNER) == DFS_FAIL) {
  // //     printf("FATAL ERROR: Unable to assign attributes to inode\n");
  // //     GracefulExit();
  // //   }



  // //   test_char++;

  // //   //This is to free it later
  // //   test_root_child.root_inode[i] = root_inode_handle;
  // //   test_root_child.child_inode[i] = inode_handle;
    


  // //   root_inode_handle = inode_handle;
  // //   if(SetCwd(root_inode_handle) == DFS_FAIL) {
  // //     printf("FATAL ERROR: Unable to change cwd\n");
  // //     GracefulExit();
  // //   }
  // // }

  // // // test 1: Delete Inode given the parents
  // // for(i = 4; i >= 0; i--)
  // // {
  // //   check = DfsDeleteInodeGivenParent(test_root_child.child_inode[i], 0);
  // //   printf("%d\n",check);
  // // }

  // // test 2: create (DFS_MAX_DIRECTORY_ENTRIES * 10) + 1 
  // // directories in root directory to explore indirect block table
  // root_inode_handle = 0;
  // for(i = 0; i < DFS_MAX_DIRECTORY_ENTRIES * 5 + 1; i++) 
  // {
  //   // get free virtual block num and directory entry
  //   test = DfsReturnBlkNumAndDirEntry(test_filename);

  //   if(test.dir_num == DFS_FAIL) 
  //   {
  //     printf("FATAL ERROR: Unable to find available blocknum and entry for %c\n", test_char);
  //     GracefulExit();
  //   }

  //   // obtain the filesystem block num for the virtual block in cwd
  //   fs_block = DfsInodeTranslateVirtualToFilesys(root_inode_handle, test.block);

  //   // create inode for the new created directory
  //   inode_handle = DfsAssignFilenameAndInodeToDirectoryEntries(test_filename, fs_block, test.dir_num);
  //   if(inode_handle == DFS_FAIL) {
  //     printf("FATAL ERROR: Unable to assign filename and inode number to a directory entry\n");
  //     GracefulExit();
  //   }

  //   // Change the attributes for new directory
  //   if(DfsAssignAttributesToInode(root_inode_handle, inode_handle, DFS_TYPE_DIR, (DFS_READ | DFS_WRITE | DFS_EXECUTE) << DFS_OWNER) == DFS_FAIL) {
  //     printf("FATAL ERROR: Unable to assign attributes to inode\n");
  //     GracefulExit();
  //   }

  //   // increment filename
  //   test_filename[0] += 1;
  // }


  //   for(i = 0; i < DFS_MAX_DIRECTORY_ENTRIES * 5 + 1; i++) 
  // {
  //   // get free virtual block num and directory entry
  //   test = DfsReturnBlkNumAndDirEntry(test_filename);

  //   if(test.dir_num == DFS_FAIL) 
  //   {
  //     printf("FATAL ERROR: Unable to find available blocknum and entry for %c\n", test_char);
  //     GracefulExit();
  //   }

  //   // obtain the filesystem block num for the virtual block in cwd
  //   fs_block = DfsInodeTranslateVirtualToFilesys(root_inode_handle, test.block);

  //   // create inode for the new created directory
  //   inode_handle = DfsAssignFilenameAndInodeToDirectoryEntries(test_filename, fs_block, test.dir_num);
  //   if(inode_handle == DFS_FAIL) {
  //     printf("FATAL ERROR: Unable to assign filename and inode number to a directory entry\n");
  //     GracefulExit();
  //   }

  //   // Change the attributes for new directory
  //   if(DfsAssignAttributesToInode(root_inode_handle, inode_handle, DFS_TYPE_FILE, (DFS_READ | DFS_WRITE | DFS_EXECUTE) << DFS_OWNER) == DFS_FAIL) {
  //     printf("FATAL ERROR: Unable to assign attributes to inode\n");
  //     GracefulExit();
  //   }

  //   // increment filename
  //   test_filename[0] += 1;
  // }

  // printf("Attempting to print cwd = 0\n");
  // SetCwd(0);
  // DfsPrintCwdPath();
  // printf("Attempting to print cwd = 1\n");
  // SetCwd(1);
  // DfsPrintCwdPath();
  // printf("Attempting to print cwd = 2\n");
  // SetCwd(2);
  // DfsPrintCwdPath();
  // printf("Done testing printcwd, returning cwd to root\n");
  // SetCwd(0);
}

